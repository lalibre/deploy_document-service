#!/bin/python3

# Datos a solicitar:
# ==================
# Nombre de la organización
# Dominio del servicio
# Puerto interno para el servicio

# Pasos:
# ======
# 1) Crear archivo de servicio
# 2) Escribir configuración http apache
# 3) Escribir configuración https apache
# 4) Instalar, activar e iniciar el servicio
# 5) Probar configuración apache
# 6) Reiniciar servicio apache
# 7) Generar certificado https
# 8) Imprimir mensaje de configuración

# Rutas de los archivos:
# ======================
# Servicio:     /etc/systemd/system/docker_ds-[SUBDOMAIN].service
# Apache http : /etc/apache2/sites-available/000-default.conf 
# Apache https: /etc/apache2/sites-available/000-default-le-ssl.conf

# Comandos:
# certbot  --apache -d [SUBDOMAIN]

from helpers import *
from os import system


welcomeMsg  = "\n"
welcomeMsg += "========================================\n"
welcomeMsg += "                                        \n"
welcomeMsg += "       GENERADOR DE CONFIGURACIÓN       \n"
welcomeMsg += "                                        \n"
welcomeMsg += "========================================\n"

print(welcomeMsg)

orgName =    input("Nombre de la organización: ")
orgDomain =  input("Subdominio:                ")
servicePort= input("Puerto:                    ")

# httpConfigFile = "/etc/apache2/sites-available/000-default.conf"
# httpsConfigFile = "/etc/apache2/sites-available/000-default-le-ssl.conf"
# serviceFile = "/etc/systemd/system/docker_ds-%s.service" % (orgDomain)

httpConfigFile = "000-default.conf"
httpsConfigFile = "000-default-le-ssl.conf"
serviceFile = "docker_ds-%s.service" % (orgDomain)

#
# Se crea el archivo para el servicio 
#


password = password_generator()
serviceString = service_string() % (orgName, password, servicePort)
file_writer(serviceFile, serviceString, 'w')

#
# La configuración http 
#

httpString = http_string()
httpString = httpString.replace("SUBDOMAIN", orgDomain)
file_writer(httpConfigFile, httpString, 'a')

#
# La configuración httpS 
#

httpsString = https_string()
httpsString = httpsString.replace("SUBDOMAIN", orgDomain)
httpsString = httpsString.replace("PORT", servicePort)
file_writer(httpsConfigFile, httpsString, 'a')

#
# Aplicar cambios
#

print("\n")
print("Salida apachectrl configtest: \n")
print(system("apachectrl configtest"))
print("\n")

choice = input("Reiniciar los servicios e instalar cert? (s/n): ")
if choice != "s":
    print("Se termina el proceso")
    exit()

print(system("systemctl restart apache2"))
print(system("certbot --apache -d %s" %(orgDomain)))

#
# Imprime datos de servicio
#

print("\n")
print("Servicio  : https://%s \n" % (orgDomain))
print("Contraseña: %s \n" % (password))
print("\n")