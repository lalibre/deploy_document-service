#!/bin/python3

from random import shuffle
from string import ascii_letters, digits, punctuation


def password_generator(password_length=12):
    characters =  list(ascii_letters + digits) # + punctuation) TODO
    shuffle(characters)
    password = "".join(characters)
    password = password[:password_length]
    return password


def service_string():
    return """[Unit]
Description=DocumentServer service for %s
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
ExecStartPre=-/usr/bin/docker exec %%n stop
ExecStartPre=-/usr/bin/docker rm %%n
ExecStartPre=/usr/bin/docker pull onlyoffice/documentserver:latest
ExecStart=/usr/bin/docker run --rm --name %%n \\
    -e JWT_ENABLED="true" \\
    -e JWT_SECRET="%s" \\
    -v /root/documentserver/data:/var/www/onlyoffice/Data \\
    -v /root/documentserver/log:/var/log/onlyoffice \\
    -p 127.0.0.1:%s:80 \\
       onlyoffice/documentserver:latest

[Install]
WantedBy=default.target
"""


def http_string():
    return """

<VirtualHost SUBDOMAIN:80>
    Protocols h2 h2c http/1.1
    ServerAdmin jfinlay@riseup.net
    ServerName SUBDOMAIN
    ServerAlias SUBDOMAIN
    ErrorLog ${APACHE_LOG_DIR}/error-SUBDOMAIN.log
    # Possible values include: debug, info, notice, warn, error, crit,                                                                     
    # alert, emerg.                                                                                                                        
    LogLevel warn

    CustomLog ${APACHE_LOG_DIR}/access-SUBDOMAIN.log combined
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =SUBDOMAIN
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

"""


def https_string():
    return """

<IfModule mod_ssl.c>
    <VirtualHost SUBDOMAIN:443>
        Protocols h2 h2c http/1.1
        ServerAdmin jfinlay@riseup.net
        ServerName SUBDOMAIN
        ServerAlias SUBDOMAIN
        ErrorLog ${APACHE_LOG_DIR}/error-SUBDOMAIN.log

        # Possible values include: debug, info, notice, warn, error, crit,                                                                 
        # alert, emerg.                                                                                                                    
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access-SUBDOMAIN.log combined

        SetEnvIf Host "^(.*)$" THE_HOST=$1
        RequestHeader setifempty X-Forwarded-Proto https
        RequestHeader setifempty X-Forwarded-Host %{THE_HOST}e
        ProxyAddHeaders Off
        ProxyPreserveHost On
        ProxyPassMatch (.*)(\/websocket)$ "ws://127.0.0.1:PORT/$1$2"
        ProxyPass / http://127.0.0.1:PORT/
        ProxyPassReverse / http://127.0.0.1:PORT/

    </VirtualHost>
</IfModule>

"""

def file_writer(configFile, configString, editMode='w'):
    fp = open(configFile, editMode)
    fp.write(configString)
    fp.close()