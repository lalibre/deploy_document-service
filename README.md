1. Crear subdominio que apunte al servidor que tendrá el servicio
2. Definir el puerto que será usado por el servicio
3. Generar los fragmentos de configuración ejecutando `main.py` será requerida la información antes indicada
4. Copiar los fragmentos de archivos correspondientes `000-default*` 
5. Crear archivo de servicio OO en `/etc/systemd/system/ARCHIVO-SERVICIO.service`
6. Registrar servicio al inicio del sistema: `systemctl enable ARCHIVO-SERVICIO`
7. Recargar y reiniciar apache
8. Generar certificados `certbot  --apache -d SUBDOMINIO`

Probar servicio
